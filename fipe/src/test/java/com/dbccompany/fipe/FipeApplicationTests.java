package com.dbccompany.fipe;

import com.dbccompany.fipe.document.Veiculo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FipeApplicationTests {

	@Test
	public void contextLoads() {
		RestTemplate restTemplate = new RestTemplate();

//		http://fipeapi.appspot.com/api/1/carros/marcas.json
//		http://fipeapi.appspot.com/api/1/[tipo]/[acao]/[parametros].json

		UriComponents uri = UriComponentsBuilder.newInstance()
				.scheme("https")
				.host("fipeapi.appspot.com")
				.path("/api/1/carros")
				.queryParam("marcas", "21")
				.build();

		String url = "http://fipeapi.appspot.com/api/1/carros/veiculo/21/4828/2013-1.json";

		ResponseEntity<Veiculo> entity = restTemplate.getForEntity(url, Veiculo.class);

		System.out.println("Modelo: " + entity.getBody().getName() +  "\n Preço: " + entity.getBody().getPreco());
	}

}
