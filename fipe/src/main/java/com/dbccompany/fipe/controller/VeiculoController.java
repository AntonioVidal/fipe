package com.dbccompany.fipe.controller;

import com.dbccompany.fipe.document.Veiculo;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping(path = "/api")
public class VeiculoController {

    @GetMapping(value = "/getVeiculos/{id}")
    @ResponseBody
    public Veiculo[] obterVeiculos(@PathVariable long id) throws Exception {
        RestTemplate restTemplate = new RestTemplate();
        String uri = "http://fipeapi.appspot.com/api/1/carros/veiculos/" + id + ".json";
        Veiculo[] veiculos = restTemplate.getForObject(uri, Veiculo[].class);

        return veiculos;
    }
}
