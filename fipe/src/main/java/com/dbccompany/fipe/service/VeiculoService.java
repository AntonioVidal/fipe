package com.dbccompany.fipe.service;

import com.dbccompany.fipe.document.Veiculo;

import java.util.List;

public interface VeiculoService {
    List<Veiculo> listarTodos();

    Veiculo listarPorId(String id);

    Veiculo salvar(Veiculo veiculo);
}
