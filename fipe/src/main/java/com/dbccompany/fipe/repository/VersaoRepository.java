package com.dbccompany.fipe.repository;
import com.dbccompany.fipe.document.Versao;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VersaoRepository extends MongoRepository<Versao, String> {
}

