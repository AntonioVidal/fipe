package com.dbccompany.fipe.service;

import com.dbccompany.fipe.document.Versao;
import com.dbccompany.fipe.repository.VersaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class VersaoService {

    @Autowired
    private VersaoRepository versaoRepository;

    @Transactional(rollbackFor = Exception.class)
    public Boolean salvar(Versao[] versoes) throws Exception {
        for (Versao versao : versoes
        ) {
            versaoRepository.save(versao);
        }
        return true;
    }
}
