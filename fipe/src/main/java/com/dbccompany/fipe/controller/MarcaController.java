package com.dbccompany.fipe.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.io.Serializable;

@RestController
public class MarcaController implements Serializable {

    @GetMapping(value = "/getMarcas")
    public br.com.dbccompany.fipe.entity.Marca[] obterMarcas() throws Exception {
        RestTemplate restTemplate = new RestTemplate();
        String uri = "http://fipeapi.appspot.com/api/1/carros/marcas.json";

        br.com.dbccompany.fipe.entity.Marca[] marcas = restTemplate.getForObject(uri, br.com.dbccompany.fipe.entity.Marca[].class);
        return marcas;
    }

}